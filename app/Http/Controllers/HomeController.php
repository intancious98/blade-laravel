<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('adminlte.master');
    }
    public function table()
    {
        return view('item.table');
    }
    public function datable()
    {
        return view('item.datable');
    }
}
